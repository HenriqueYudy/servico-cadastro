package com.cadastro.cadastro.domain.resource;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cadastro.cadastro.domain.model.Fornecedor;
import com.cadastro.cadastro.domain.repository.FornecedorRepository;

@RestController
@RequestMapping(value = "/fornecedor")
public class FornecedorResource {

	@Autowired
	private FornecedorRepository fornecedorRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Fornecedor> addFornecedor(@RequestBody Fornecedor fornecedor) {

		Fornecedor newFornecedor = fornecedorRepository.save(fornecedor);

		return ResponseEntity.status(HttpStatus.CREATED).body(newFornecedor);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<Fornecedor>> getClientes() {

		List<Fornecedor> lstFornecedor = fornecedorRepository.findAll();

		if (lstFornecedor.isEmpty()) {
			return new ResponseEntity<List<Fornecedor>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Fornecedor>>(lstFornecedor, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Fornecedor> getCliente(@PathVariable("id") long id) {

		Fornecedor capFornecedor = fornecedorRepository.findOne(id);

		if (capFornecedor == null) {
			return new ResponseEntity<Fornecedor>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Fornecedor>(capFornecedor, HttpStatus.OK);
	}
}
